var wms_layers = [];


        var lyr_GoogleSatellite_0 = new ol.layer.Tile({
            'title': 'Google Satellite',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' &middot; <a href="https://www.google.at/permissions/geoguidelines/attr-guide.html">Map data ©2015 Google</a>',
                url: 'https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}'
            })
        });
var format_SetoresCensitrios_1 = new ol.format.GeoJSON();
var features_SetoresCensitrios_1 = format_SetoresCensitrios_1.readFeatures(json_SetoresCensitrios_1, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_SetoresCensitrios_1 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_SetoresCensitrios_1.addFeatures(features_SetoresCensitrios_1);
var lyr_SetoresCensitrios_1 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_SetoresCensitrios_1, 
                style: style_SetoresCensitrios_1,
                interactive: true,
                title: '<img src="styles/legend/SetoresCensitrios_1.png" /> Setores Censitários'
            });
var format_Ruas_2 = new ol.format.GeoJSON();
var features_Ruas_2 = format_Ruas_2.readFeatures(json_Ruas_2, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Ruas_2 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_Ruas_2.addFeatures(features_Ruas_2);
var lyr_Ruas_2 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Ruas_2, 
                style: style_Ruas_2,
                interactive: false,
                title: '<img src="styles/legend/Ruas_2.png" /> Ruas'
            });
var format_30052020_3 = new ol.format.GeoJSON();
var features_30052020_3 = format_30052020_3.readFeatures(json_30052020_3, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_30052020_3 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_30052020_3.addFeatures(features_30052020_3);
var lyr_30052020_3 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_30052020_3, 
                style: style_30052020_3,
                interactive: true,
                title: '<img src="styles/legend/30052020_3.png" /> 30-05-2020'
            });
var format_28052020_4 = new ol.format.GeoJSON();
var features_28052020_4 = format_28052020_4.readFeatures(json_28052020_4, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_28052020_4 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_28052020_4.addFeatures(features_28052020_4);
var lyr_28052020_4 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_28052020_4, 
                style: style_28052020_4,
                interactive: true,
                title: '<img src="styles/legend/28052020_4.png" /> 28-05-2020'
            });
var format_28042020_5 = new ol.format.GeoJSON();
var features_28042020_5 = format_28042020_5.readFeatures(json_28042020_5, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_28042020_5 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_28042020_5.addFeatures(features_28042020_5);
var lyr_28042020_5 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_28042020_5, 
                style: style_28042020_5,
                interactive: true,
                title: '<img src="styles/legend/28042020_5.png" /> 28-04-2020'
            });

lyr_GoogleSatellite_0.setVisible(true);lyr_SetoresCensitrios_1.setVisible(true);lyr_Ruas_2.setVisible(true);lyr_30052020_3.setVisible(true);lyr_28052020_4.setVisible(false);lyr_28042020_5.setVisible(false);
var layersList = [lyr_GoogleSatellite_0,lyr_SetoresCensitrios_1,lyr_Ruas_2,lyr_30052020_3,lyr_28052020_4,lyr_28042020_5];
lyr_SetoresCensitrios_1.set('fieldAliases', {'ID': 'ID', 'CD_GEOCODI': 'CD_GEOCODI', 'TIPO': 'TIPO', 'CD_GEOCODB': 'CD_GEOCODB', 'NM_BAIRRO': 'NM_BAIRRO', 'CD_GEOCODS': 'CD_GEOCODS', 'CD_GEOCODD': 'CD_GEOCODD', 'CD_GEOCODM': 'CD_GEOCODM', });
lyr_Ruas_2.set('fieldAliases', {'osm_id': 'osm_id', 'name': 'name', 'highway': 'highway', 'waterway': 'waterway', 'aerialway': 'aerialway', 'barrier': 'barrier', 'man_made': 'man_made', 'z_order': 'z_order', 'other_tags': 'other_tags', });
lyr_30052020_3.set('fieldAliases', {'Y': 'Y', 'N_casos': 'N_casos', });
lyr_28052020_4.set('fieldAliases', {'Y': 'Y', 'N_casos': 'N_casos', });
lyr_28042020_5.set('fieldAliases', {'N_casos': 'N_casos', 'Y': 'Y', });
lyr_SetoresCensitrios_1.set('fieldImages', {'ID': 'TextEdit', 'CD_GEOCODI': 'TextEdit', 'TIPO': 'TextEdit', 'CD_GEOCODB': 'TextEdit', 'NM_BAIRRO': 'TextEdit', 'CD_GEOCODS': 'TextEdit', 'CD_GEOCODD': 'TextEdit', 'CD_GEOCODM': 'TextEdit', });
lyr_Ruas_2.set('fieldImages', {'osm_id': 'TextEdit', 'name': 'TextEdit', 'highway': 'TextEdit', 'waterway': 'TextEdit', 'aerialway': 'TextEdit', 'barrier': 'TextEdit', 'man_made': 'TextEdit', 'z_order': 'TextEdit', 'other_tags': 'TextEdit', });
lyr_30052020_3.set('fieldImages', {'Y': 'TextEdit', 'N_casos': 'TextEdit', });
lyr_28052020_4.set('fieldImages', {'Y': 'TextEdit', 'N_casos': 'TextEdit', });
lyr_28042020_5.set('fieldImages', {'N_casos': 'TextEdit', 'Y': 'TextEdit', });
lyr_SetoresCensitrios_1.set('fieldLabels', {'ID': 'no label', 'CD_GEOCODI': 'no label', 'TIPO': 'no label', 'CD_GEOCODB': 'no label', 'NM_BAIRRO': 'no label', 'CD_GEOCODS': 'no label', 'CD_GEOCODD': 'no label', 'CD_GEOCODM': 'no label', });
lyr_Ruas_2.set('fieldLabels', {'osm_id': 'header label', 'name': 'no label', 'highway': 'no label', 'waterway': 'no label', 'aerialway': 'no label', 'barrier': 'no label', 'man_made': 'no label', 'z_order': 'no label', 'other_tags': 'no label', });
lyr_30052020_3.set('fieldLabels', {'Y': 'header label', 'N_casos': 'header label', });
lyr_28052020_4.set('fieldLabels', {'Y': 'no label', 'N_casos': 'header label', });
lyr_28042020_5.set('fieldLabels', {'N_casos': 'no label', 'Y': 'no label', });
lyr_28042020_5.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});